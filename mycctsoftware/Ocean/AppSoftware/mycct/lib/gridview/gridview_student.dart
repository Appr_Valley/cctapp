import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';



class GridviewStud extends StatefulWidget {
  @override
  _GridviewStudState createState() => new _GridviewStudState();
}

class _GridviewStudState extends State<GridviewStud> {

  @override
  Widget build(BuildContext context) {

    var spacecrafts = ["NSFAS","SAQA","MOT","DHET","TVET Colleges","Career Planet","PACE Careers","Student Health"];

    var myGridView = new GridView.builder(
      itemCount: spacecrafts.length,
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
      itemBuilder: (BuildContext context, int index) {
        return new GestureDetector(
          child: new Card(
            elevation: 5.0,
            color: Colors.deepOrange,
            child: new Container(
              alignment: Alignment.center,
              margin: new EdgeInsets.only(top: 10.0, bottom: 10.0,left: 5.0),
              child: new Text(spacecrafts[index],style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white,),),
            ),
          ),



          onTap: () async {

            String grade = (spacecrafts[index]);

            switch (grade) {

              case 'NSFAS':
              if (await canLaunch("https://nsfasapplication.co.za")) {
                await launch("https://nsfasapplication.co.za");
              }
              break;

              case 'SAQA':
                if (await canLaunch("http://www.saqa.org.za")) {
                  await launch("http://www.saqa.org.za");
                }
                break;

              case 'MOT':
                if (await canLaunch("http://www.mot.org.za")) {
                  await launch("http://www.mot.org.za");
                }
                break;

              case 'DHET':
                if (await canLaunch("http://www.dhet.gov.za")) {
                  await launch("http://www.dhet.gov.za");
                }
                break;

              case 'TVET Colleges':
                if (await canLaunch("http://www.tvetcolleges.co.za")) {
                  await launch("http://www.tvetcolleges.co.za");
                }
                break;

              case 'Career Planet':
                if (await canLaunch("http://www.careerplanet.co.za")) {
                  await launch("http://www.careerplanet.co.za");
                }
                break;

              case 'PACE Careers':
                if (await canLaunch("http://www.pacecareers.com")) {
                  await launch("http://www.pacecareers.com");
                }
                break;

              case 'Student Health':
                if (await canLaunch("http://www.studenthealth.co.za")) {
                  await launch("http://www.studenthealth.co.za");
                }
                break;

              default:
                print("Invalid Grade");
            }


            //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ContactUs()));

          },
        );
      },
    );

    return new Scaffold(
      appBar: new AppBar(
          title: new Text("Social Media Links")
      ),
      body: myGridView,
    );
  }
}
