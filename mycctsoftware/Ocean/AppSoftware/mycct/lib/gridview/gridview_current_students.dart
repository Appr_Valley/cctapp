import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';



class GridviewCStud extends StatefulWidget {
  @override
  _GridviewCStudState createState() => new _GridviewCStudState();
}

class _GridviewCStudState extends State<GridviewCStud> {

  @override
  Widget build(BuildContext context) {

    var spacecrafts = ["CCT Website","Enterprise","Hubble","Kepler","Juno","Casini","Atlantis"];

    var myGridView = new GridView.builder(
      itemCount: spacecrafts.length,
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
      itemBuilder: (BuildContext context, int index) {
        return new GestureDetector(
          child: new Card(
            elevation: 5.0,
            color: Colors.deepOrange,
            child: new Container(
              alignment: Alignment.center,
              margin: new EdgeInsets.only(top: 10.0, bottom: 10.0,left: 5.0),
              child: new Text(spacecrafts[index],style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white,),),
            ),
          ),
          onTap: () {

            String grade = (spacecrafts[index]);

            switch (grade) {

              case 'CCT Website':

                print("Good enough. But work hard");

                break;

              case 'Enterprise':
              //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ContactUs()));
                break;

              case 'C':
                print("Good enough. But work hard");
                break;

              case 'F':
                print("You have failed");
                break;
              default:
                print("Invalid Grade");
            }


            //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ContactUs()));

            
          },
        );
      },
    );

    return new Scaffold(
      appBar: new AppBar(
          title: new Text("Social Media Links")
      ),
      body: myGridView,
    );
  }
}
