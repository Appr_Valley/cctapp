

import 'package:shared_preferences/shared_preferences.dart';

class MyPreferences{
  static const  AUTOMATIC = "automatic";
  static const  USERNAME = "username";
  static const  PASSWORD = "password";
  static const  FIRSTNAME = "first_names";
  static const  COLLEGEID = "college_id";
  static const  CAMPUSNO = "campusno";
  static const  USERTYPENO = "usertypeno";

  static final MyPreferences instance = MyPreferences._internal();


  //Campos a manejar
  SharedPreferences _sharedPreferences;
  bool automatic = false;
  String username = "";
  String password = "";
  String firstnames = "";
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;

  MyPreferences._internal(){

  }

  factory MyPreferences()=>instance;

  Future<SharedPreferences> get preferences async{
    if(_sharedPreferences != null){
      return _sharedPreferences;
    }else{
      _sharedPreferences = await SharedPreferences.getInstance();
      automatic = _sharedPreferences.getBool(AUTOMATIC);

      username = _sharedPreferences.getString(USERNAME);
      password = _sharedPreferences.getString(PASSWORD);
      firstnames = _sharedPreferences.getString(FIRSTNAME);
      college_id = _sharedPreferences.getInt(COLLEGEID);
      campusno = _sharedPreferences.getInt(CAMPUSNO);
      usertypeno = _sharedPreferences.getInt(USERTYPENO);

      if(automatic == null){
        automatic = false;
        username = "";
        password = "";
        firstnames = "";
        college_id = 0;
        campusno = 0;
        usertypeno = 0;

      }
      return _sharedPreferences;

    }

  }
  Future<bool> commit() async {
    await _sharedPreferences.setBool(AUTOMATIC, automatic);
    await _sharedPreferences.setString(USERNAME, username);
    await _sharedPreferences.setString(PASSWORD, password);
    await _sharedPreferences.setString(FIRSTNAME, firstnames);
    await _sharedPreferences.setInt(COLLEGEID, college_id);
    await _sharedPreferences.setInt(CAMPUSNO, campusno);
    await _sharedPreferences.setInt(USERTYPENO, usertypeno);

  }

  Future<MyPreferences> init() async{
    _sharedPreferences = await preferences;
    return this;
  }


}