
import 'package:mycct/models/db_user.dart';
import 'package:mycct/models/database_creator.dart';

class RepositoryServiceTodo {
  static Future<List<Todo>> getAllTodos() async {
    final sql = '''SELECT * FROM ${DatabaseCreator.userTable}
    ''';
    final data = await db.rawQuery(sql);
    List<Todo> todos = List();

    for (final node in data) {
      final todo = Todo.fromJson(node);
      todos.add(todo);
    }
    return todos;
  }

  static Future<Todo> getTodo(int id) async {
    //final sql = '''SELECT * FROM ${DatabaseCreator.todoTable}
    //WHERE ${DatabaseCreator.id} = $id''';
    //final data = await db.rawQuery(sql);

    final sql = '''SELECT * FROM ${DatabaseCreator.userTable}
    WHERE ${DatabaseCreator.username} = ?''';

    List<dynamic> params = [id];
    final data = await db.rawQuery(sql, params);

    final todo = Todo.fromJson(data.first);
    return todo;
  }

  static Future<void> addTodo(Todo todo) async {
    final sql = '''INSERT INTO ${DatabaseCreator.userTable}
    (
      ${DatabaseCreator.username},
      ${DatabaseCreator.password},
      ${DatabaseCreator.firstnames},
      ${DatabaseCreator.collegeid},
      ${DatabaseCreator.campusno},
      ${DatabaseCreator.usertypeno}
     
    )
    VALUES (?,?,?,?)''';
    List<dynamic> params = [todo.username, todo.password, todo.firstnames, todo.collegeid, todo.campusno, todo.usertypeno];
    final result = await db.rawInsert(sql, params);
    DatabaseCreator.databaseLog('Add user', sql, null, result, params);
  }

  static Future<void> updateTodo(Todo todo) async {
    /*final sql = '''UPDATE ${DatabaseCreator.todoTable}
    SET ${DatabaseCreator.name} = "${todo.name}"
    WHERE ${DatabaseCreator.id} = ${todo.id}
    ''';*/

    final sql = '''UPDATE ${DatabaseCreator.userTable}
    SET ${DatabaseCreator.firstnames} = ?
    WHERE ${DatabaseCreator.username} = ?
    ''';

    List<dynamic> params = [todo.firstnames, todo.username];
    final result = await db.rawUpdate(sql, params);

    DatabaseCreator.databaseLog('Update todo', sql, null, result, params);
  }

}