
import 'package:mycct/models/database_creator.dart';

class Todo {
  int username;
  String password;
  String firstnames;
  int collegeid;
  int campusno;
  int usertypeno;

  Todo(this.username, this.password, this.firstnames, this.collegeid, this.campusno, this.usertypeno);

  Todo.fromJson(Map<String, dynamic> json) {
    this.username = json[DatabaseCreator.username];
    this.password = json[DatabaseCreator.password];
    this.firstnames = json[DatabaseCreator.firstnames];
    this.collegeid = json[DatabaseCreator.collegeid];
    this.campusno = json[DatabaseCreator.campusno];
    this.usertypeno = json[DatabaseCreator.usertypeno];

  }
}