import 'package:flutter/material.dart';
import 'package:mycct/progress_pages/progress_faqs.dart';
import 'package:mycct/student_pages/student_profile_data.dart';


class SLoggedMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Current Students"),),
      body: ListView(
        children: <Widget>[
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("Student Login",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white,),),
              trailing: Icon(Icons.info,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new StudProfile()));
              },
            ),
          ),
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("Semester Dates",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white, ),),
              trailing: Icon(Icons.calendar_today,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new FAQS()));
              },
            ),
          ),
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("College Rules",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white, ),),
              trailing: Icon(Icons.forum,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new FAQS()));
              },
            ),

          ),
        ],
      ),
    );
  }

}
