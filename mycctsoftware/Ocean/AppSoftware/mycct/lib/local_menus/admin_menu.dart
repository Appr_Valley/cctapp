import 'package:flutter/material.dart';
import 'package:mycct/progress_pages/progress_faqs.dart';
import 'package:mycct/messaging/mess_main.dart';


class AdminMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Admin Menu"),),
      body: ListView(
        children: <Widget>[
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("Send Push Notifications to students",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white,),),
              trailing: Icon(Icons.info,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MessMainPage()));
              },
            ),
          ),
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("College Rules",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white, ),),
              trailing: Icon(Icons.forum,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new FAQS()));
              },
            ),

          ),
        ],
      ),
    );
  }

}
