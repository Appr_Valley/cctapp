import 'package:flutter/material.dart';
import 'package:mycct/student_pages/student_prof_progress_page.dart';
import 'package:mycct/gridview/gridview_student.dart';
import 'package:mycct/main.dart';
import 'package:mycct/progress_pages/public_holidays.dart';

class StudLoggedMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Current Students"),),
      body: ListView(
        children: <Widget>[
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("My Baseline Data",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white,),),
              trailing: Icon(Icons.info,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ProfPage()));
              },
            ),
          ),
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("Student Menu",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white, ),),
              trailing: Icon(Icons.calendar_today,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new GridviewStud()));
              },
            ),
          ),
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("Public and College Holidays",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white, ),),
              trailing: Icon(Icons.calendar_today,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new Holidays()));
              },
            ),
          ),
          new Card(
            color: Colors.red,
            child: ListTile(
              title: Text("Exit Student Menu",style: new TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white, ),),
              trailing: Icon(Icons.calendar_today,color: Colors.white,),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MyApp()));
              },
            ),
          ),
        ],
      ),
    );
  }

}
