
import 'package:flutter/material.dart';
import 'package:mycct/public_pages/drawer/card_about.dart';




class StudMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            Image.asset('images/welcome/welcome.jpg'),
            new ListTile(
                title: new Text("College Course Information"),
                trailing: new Icon(Icons.info,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CardAboutPage()));
                }
            ),
            new ListTile(
              title: new Text("Close"),
              trailing: new Icon(Icons.closed_caption,color: Colors.redAccent),
              onTap: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      ),
      body: DefaultTabController(
        length: 6,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                  title: new Text('Prospective Students'),
                  expandedHeight: 500.0,
                  floating: false,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    //centerTitle: true,
                    background:
                    new Image.asset('images/welcome/welcome1.jpg',fit: BoxFit.cover),
                  )
              ),
            ];
          },
          body: ListView(
            children: [
              Image.asset(
                'images/learnership.jpg',
              ),
              new Divider(),
              Image.asset(
                'images/aspire.jpg',
              ),

              new Divider(),
              Image.asset(
                'images/nsfas.jpg',
              ),
              new Divider(),
            ],
          ),

        ),
      ),
    );
  }
}


