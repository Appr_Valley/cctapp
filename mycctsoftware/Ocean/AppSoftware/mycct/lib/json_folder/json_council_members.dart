
class Note {
  String membername;
  String urlLocation;
  String salutation;


  Note(this.membername, this.urlLocation, this.salutation);

  Note.fromJson(Map<String, dynamic> json) {

    membername = json['membername'];
    urlLocation = json['pic_url'];
    salutation = json['salutation'];
  }
}
