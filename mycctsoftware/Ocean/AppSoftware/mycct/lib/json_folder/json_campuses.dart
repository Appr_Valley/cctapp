
class Note {
  String campusName;
  String telephone;
  String offering1;
  String offering2;
  String offering3;
  String urlLocation;


  Note(this.campusName, this.telephone, this.offering1, this.offering2, this.offering3, this.urlLocation);

  Note.fromJson(Map<String, dynamic> json) {
    campusName = json['campus_name'];
    telephone = json['telephone'];
    offering1 = json['offering1'];
    offering2 = json['offering2'];
    offering3 = json['offering3'];
    urlLocation = json['url_location'];
  }
}



