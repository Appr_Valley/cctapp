
class Note {
  String username;
  String firstname;
  String surname;
  String idno;
  String qualName;
  String mobileno;

  Note(this.username, this.firstname, this.surname, this.idno, this.qualName, this.mobileno);

  Note.fromJson(Map<String, dynamic> json) {

    username = json['studentno'];
    firstname = json['first_names'];
    surname = json['surname'];
    idno = json['idno'];
    qualName = json['qualification_name'];
    mobileno = json['mobileno'];
  }
}


