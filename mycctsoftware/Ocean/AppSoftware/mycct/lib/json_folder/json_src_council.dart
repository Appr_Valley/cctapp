
class Note {
  String memberName;
  String urlLocation;

  Note(this.memberName, this.urlLocation);

  Note.fromJson(Map<String, dynamic> json) {
    memberName = json['member_name'];
    urlLocation = json['url_location'];
  }
}
