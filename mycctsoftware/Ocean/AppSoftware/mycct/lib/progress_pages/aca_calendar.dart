

import 'package:flutter/material.dart';
import 'package:mycct/utilities/api_services.dart';

class AcaCal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Academic Calendar'),),
      body: FutureBuilder(
        future: ApiService.getAcaCal(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final posts = snapshot.data;
            return ListView.separated(
              separatorBuilder: (context, index) {
                return Divider(height: 2, color: Colors.black,);
              },
              itemBuilder: (context, index) {
                return Card(

                  elevation: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          posts[index]['progdescription'],
                          style: TextStyle(color: Colors.pinkAccent, fontWeight: FontWeight.bold,fontSize: 16.0),
                        ),
                        Text(
                          posts[index]['term'],
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,fontSize: 14.0),
                        ),
                        Text("Staff Commence Date: " +
                            posts[index]['staff_commence_date'],
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,fontSize: 12.0),
                        ),
                        Text("Class Commence Date: " +
                            posts[index]['class_commence_date'],
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,fontSize: 12.0),
                        ),
                        Text("Class End Date: " +
                            posts[index]['class_enddate'],
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,fontSize: 12.0),
                        ),
                        Text("College Close Date: " +
                            posts[index]['college_close_date'],
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: posts.length,
            );
          }
          return Center(child: CircularProgressIndicator(),);
        },
      ),
    );
  }
}