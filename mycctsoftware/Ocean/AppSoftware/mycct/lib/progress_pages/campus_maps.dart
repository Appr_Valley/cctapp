import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'dart:convert';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';


class SpacecraftM {
  final int campusno;
  final String campusName, telephone, offering1, offering2, offering3, urlLocation;
  final double lat, lon;
  SpacecraftM({
    this.campusno,
    this.campusName,
    this.telephone,
    this.offering1,
    this.offering2,
    this.offering3,
    this.urlLocation,
    this.lat,
    this.lon
  });
  factory SpacecraftM.fromJson(Map<String, dynamic> jsonData) {
    return SpacecraftM(
      campusno: jsonData['campusno'],
      campusName: jsonData['campus_name'],
      telephone: jsonData['telephone'],
      offering1: jsonData['offering1'],
      offering2: jsonData['offering2'],
      offering3: jsonData['offering3'],
      urlLocation: jsonData['url_location'],
      lat: jsonData['lat'],
      lon: jsonData['lon'],
    );
  }
}
class CustomListView extends StatelessWidget {
  final List<SpacecraftM> spacecrafts;
  CustomListView(this.spacecrafts);
  Widget build(context) {
    return ListView.builder(
      itemCount: spacecrafts.length,
      itemBuilder: (context, int currentIndex) {
        return createViewItem(spacecrafts[currentIndex], context);
      },
    );
  }
  Widget createViewItem(SpacecraftM spacecraft, BuildContext context) {
    return new ListTile(
        title: new Card(
          elevation: 5.0,
          child: new Container(
            decoration: BoxDecoration(border: Border.all(color: Colors.orange)),
            padding: EdgeInsets.all(0.0),
            margin: EdgeInsets.all(0.0),
            child: Card(
              elevation: 10.0,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      spacecraft.campusName,
                      style: new TextStyle(fontWeight: FontWeight.bold,color: Colors.pink),
                      textAlign: TextAlign.right,
                    ),
                    Image.asset(spacecraft.urlLocation,),
                    new Text("Telephone " +
                      spacecraft.telephone,
                      style: new TextStyle(fontWeight: FontWeight.bold,color: Colors.pink),
                      textAlign: TextAlign.right,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        onTap: () {
          var route = new MaterialPageRoute(
            builder: (BuildContext context) =>
            new SecondScreen(value: spacecraft),
          );

          Navigator.of(context).push(route);
        });
  }
}
//Future is n object representing a delayed computation.
Future<List<SpacecraftM>> downloadJSON() async {
  final jsonEndpoint =
      "http://197.242.92.154/rest/tvetcolleges/Campuses";
  final response = await get(jsonEndpoint);
  if (response.statusCode == 200) {
    List spacecrafts = json.decode(response.body);
    return spacecrafts
        .map((spacecraft) => new SpacecraftM.fromJson(spacecraft))
        .toList();
  } else
    throw Exception('We were not able to successfully download the json data.');
}

//Second Screen

class SecondScreen extends StatefulWidget {
  final SpacecraftM value;
  SecondScreen({Key key, this.value}) : super(key: key);
  @override
  _SecondScreenState createState() => _SecondScreenState();
}


class _SecondScreenState extends State<SecondScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          '${widget.value.campusName}',
          style: new TextStyle(fontWeight: FontWeight.bold),
          textAlign: TextAlign.left,
        ),
      ),
      body: new FlutterMap(
          options: new MapOptions(
              //center: LatLng(-33.928359, 18.4551738),
              center: LatLng(widget.value.lat, widget.value.lon),
              minZoom: 13.0
          ),
          layers: [
            new TileLayerOptions(
                urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                subdomains: ['a', 'b', 'c' ]
            ),
            new MarkerLayerOptions(
              markers: [
                new Marker(
                    width: 45.0,
                    height: 45.0,
                    point: new LatLng(widget.value.lat, widget.value.lon),
                    builder: (context) => new Container(
                      child: IconButton(
                        icon: Icon(Icons.location_on),
                        color: Colors.red,
                        iconSize: 45.0,
                        onPressed: () {
                          print('Marker Pressed');
                        },
                      ),
                    )
                ),
              ],
            )
          ]
      ),

    );
  }

}



class CampusMaps extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.red,
      ),
      home: new Scaffold(
        appBar: new AppBar(title: Text('College Campuses Maps')),
        body: new Center(
          child: new FutureBuilder<List<SpacecraftM>>(
            future: downloadJSON(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<SpacecraftM> spacecrafts = snapshot.data;
                return new CustomListView(spacecrafts);
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return new CircularProgressIndicator();
            },
          ),
        ),
      ),      debugShowCheckedModeBanner: false,

    );
  }
}

