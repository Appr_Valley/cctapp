

import 'package:flutter/material.dart';
import 'package:mycct/utilities/api_services.dart';

class Campuses extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('College Campuses'),),
      body: FutureBuilder(
        future: ApiService.getCampuses(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final posts = snapshot.data;
            return ListView.separated(
              separatorBuilder: (context, index) {
                return Divider(height: 2, color: Colors.black,);
              },
              itemBuilder: (context, index) {

                return new FlatButton(
                  child: Card(
                    elevation: 10.0,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            posts[index]['campus_name'],
                            style: TextStyle(color: Colors.pinkAccent, fontWeight: FontWeight.bold,fontSize: 20.0),
                          ),
                          Image.asset(posts[index]['url_location'],),

                          Text("Telephone: " +
                            posts[index]['telephone'],
                            style: TextStyle(color: Colors.pinkAccent, fontWeight: FontWeight.normal,fontSize: 14.0),
                          ),
                          Text(
                            posts[index]['offering1'],
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,fontSize: 12.0),
                          ),
                          Text(
                            posts[index]['offering2'],
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,fontSize: 12.0),
                          ),
                          Text(
                            posts[index]['offering3'],
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,fontSize: 12.0),
                          ),
                        ],

                      ),
                    ),
                  ),
                  onPressed: (){

                  },
                );
              },
              itemCount: posts.length,
            );
          }
          return Center(child: CircularProgressIndicator(),);
        },
      ),
    );
  }
}



