import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'dart:convert';



class SpacecraftA {
  final int calno, lecturing_days, lect_staff_days;
  final String examDate, programDesc, term, staff_commence_date, class_commence_date, class_enddate, college_close_date;
  SpacecraftA({
    this.calno,
    this.lecturing_days,
    this.lect_staff_days,
    this.examDate,
    this.programDesc,
    this.term,
    this.staff_commence_date,
    this.class_commence_date,
    this.class_enddate,
    this.college_close_date
  });
  factory SpacecraftA.fromJson(Map<String, dynamic> jsonData) {
    return SpacecraftA(
      calno: jsonData['calno'],
      lecturing_days: jsonData['lecturing_days'],
      lect_staff_days: jsonData['lect_staff_days'],
      examDate: jsonData['exam_date'],
      programDesc: jsonData['progdescription'],
      term: jsonData['term'],
      staff_commence_date: jsonData['staff_commence_date'],
      class_commence_date: jsonData['class_commence_date'],
      class_enddate: jsonData['class_enddate'],
      college_close_date: jsonData['college_close_date'],
    );
  }
}
class CustomListView extends StatelessWidget {
  final List<SpacecraftA> spacecrafts;
  CustomListView(this.spacecrafts);
  Widget build(context) {
    return ListView.builder(
      itemCount: spacecrafts.length,
      itemBuilder: (context, int currentIndex) {
        return createViewItem(spacecrafts[currentIndex], context);
      },
    );
  }
  Widget createViewItem(SpacecraftA spacecraft, BuildContext context) {
    return new ListTile(
        title: new Card(
          elevation: 5.0,
          child: new Container(
            decoration: BoxDecoration(border: Border.all(color: Colors.orange)),
            padding: EdgeInsets.all(0.0),
            margin: EdgeInsets.all(0.0),
            child: Card(
              elevation: 10.0,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      spacecraft.programDesc.toString(),
                      style: new TextStyle(fontWeight: FontWeight.bold,color: Colors.pink),
                      textAlign: TextAlign.right,
                    ),
                    new Text(
                      spacecraft.term,
                      style: new TextStyle(fontWeight: FontWeight.bold,color: Colors.pink),
                      textAlign: TextAlign.right,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        onTap: () {
          var route = new MaterialPageRoute(
            builder: (BuildContext context) =>
            new SecondScreen(value: spacecraft),
          );

          Navigator.of(context).push(route);
        });
  }
}
//Future is n object representing a delayed computation.
Future<List<SpacecraftA>> downloadJSON() async {
  final jsonEndpoint =
      "http://197.242.92.154/rest/tvetcolleges/Calendar";
  final response = await get(jsonEndpoint);
  if (response.statusCode == 200) {
    List spacecrafts = json.decode(response.body);
    return spacecrafts
        .map((spacecraft) => new SpacecraftA.fromJson(spacecraft))
        .toList();
  } else
    throw Exception('We were not able to successfully download the json data.');
}

//Second Screen

class SecondScreen extends StatefulWidget {
  final SpacecraftA value;
  SecondScreen({Key key, this.value}) : super(key: key);
  @override
  _SecondScreenState createState() => _SecondScreenState();
}


class _SecondScreenState extends State<SecondScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: new Text('Calendar Information')),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
              children: <Widget>[
                Text('  Staff Commence Date',
                  style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold,fontSize: 12.0),
                  softWrap: true,
                ),
                Card(
                  elevation: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          '${widget.value.staff_commence_date}'.toString(),
                          style: new TextStyle(fontWeight: FontWeight.bold),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 30),
              ]
          ),
        )
    );
  }

}


class SpaceAppA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.red,
      ),
      home: new Scaffold(
        appBar: new AppBar(title: const Text('College Academic Calendar')),
        body: new Center(
          child: new FutureBuilder<List<SpacecraftA>>(
            future: downloadJSON(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<SpacecraftA> spacecrafts = snapshot.data;
                return new CustomListView(spacecrafts);
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return new CircularProgressIndicator();
            },
          ),
        ),
      ),      debugShowCheckedModeBanner: false,

    );
  }
}

