

import 'package:flutter/material.dart';
import 'package:mycct/utilities/api_services.dart';

class Holidays extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Public and College Holidays'),),
      body: FutureBuilder(
        future: ApiService.getHolidays(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final posts = snapshot.data;
            return ListView.separated(
              separatorBuilder: (context, index) {
                return Divider(height: 2, color: Colors.black,);
              },
              itemBuilder: (context, index) {
                return Card(

                  elevation: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          posts[index]['holiday_date'.toString()],
                          style: TextStyle(color: Colors.pinkAccent, fontWeight: FontWeight.bold,fontSize: 20.0),
                        ),
                        Text(
                          posts[index]['holiday_description'],
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,fontSize: 15.0),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: posts.length,
            );
          }
          return Center(child: CircularProgressIndicator(),);
        },
      ),
    );
  }
}