import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class MapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Central Office Location'),
      ),
      body: new FlutterMap(
          options: new MapOptions(
              center: LatLng(-33.9283635, 18.4551738),
              minZoom: 13.0
          ),
          layers: [
            new TileLayerOptions(
                urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                subdomains: ['a', 'b', 'c' ]
            ),
            new MarkerLayerOptions(
              markers: [
                new Marker(
                    width: 45.0,
                    height: 45.0,
                    point: new LatLng(-33.9283635, 18.4551738),
                    builder: (context) => new Container(
                      child: IconButton(
                        icon: Icon(Icons.location_on),
                        color: Colors.red,
                        iconSize: 45.0,
                        onPressed: () {
                          print('Marker Pressed');
                        },
                      ),
                    )
                ),
              ],
            )
          ]
      ),
    );
  }
}
