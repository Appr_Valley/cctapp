


import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:mycct/json_folder/json_campuses.dart';


class CampusCardJSONPage extends StatefulWidget {

  @override
  _CampusCardJSONPageState createState() => _CampusCardJSONPageState();
}

class _CampusCardJSONPageState extends State<CampusCardJSONPage> {

  List<Note> _notes = List<Note>();

  Future<List<Note>> fetchNotes() async {

    var url = 'http://197.242.92.154/rest/tvetcolleges/Campuses';

    var response = await http.get(url);

    var notes = List<Note>();

    if (response.statusCode == 200) {
      var notesJson = json.decode(response.body);
      for (var noteJson in notesJson) {
        notes.add(Note.fromJson(noteJson));
      }
    }
    return notes;
  }

  @override
  void initState() {
    fetchNotes().then((value) {
      setState(() {
        _notes.addAll(value);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('CCT Campuses',style: TextStyle(color: Colors.white),
          ),
        ),
        body: ListView.builder(
          itemBuilder: (context, index) {
            return Card(
              elevation: 10.0,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(_notes[index].campusName,
                      style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold,fontSize: 20.0),
                    ),
                    Image.asset(_notes[index].urlLocation,
                    ),
                    Text(_notes[index].telephone,
                      style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0),
                    ),
                    Text(_notes[index].offering1,
                      style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 10.0),
                    ),
                    Text(_notes[index].offering2,
                      style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 10.0),
                    ),
                    Text(_notes[index].offering3,
                      style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 10.0),
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: _notes.length,
        )
    );
  }
}


