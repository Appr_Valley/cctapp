
//First go to pubspec.yaml file and add this => carousel_pro: ^0.0.1 under dependencies.
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:mycct/public_pages/drawer/card_principal.dart';
import 'package:mycct/public_pages/drawer/card_about.dart';
import 'package:mycct/public_pages/drawer/card_privacy.dart';
import 'package:mycct/progress_pages/progress_faqs.dart';
import 'package:mycct/social_media/sample_social_page.dart';
import 'package:mycct/gridview/gridview_student.dart';
import 'package:mycct/login/abs_login_page.dart';
import 'package:mycct/progress_pages/campus_maps.dart';
import 'package:mycct/public_pages/future_events.dart';
import 'package:mycct/public_pages/map.dart';
import 'package:mycct/progress_pages/progress_src_council.dart';
import 'package:mycct/notes_menus/prospective_menu.dart';



class HomePage extends StatefulWidget {
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  initState() {
    super.initState();
    controller = new AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    animation = new Tween(begin: 0.0, end: 18.0).animate(controller)
      ..addListener(() {
        setState(() {
          // the state that has changed here is the animation object’s value
        });
      });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    Widget carousel = new Carousel(
      boxFit: BoxFit.cover,
      images: [
        new AssetImage('images/app_pics/master.jpg'),
        new AssetImage('images/app_pics/busstudies.jpg'),
        new AssetImage('images/app_pics/art2.jpg'),
        new AssetImage('images/app_pics/building.jpg'),
        new AssetImage('images/app_pics/electrical.jpg'),
        new AssetImage('images/app_pics/haircare.jpg'),
        new AssetImage('images/app_pics/it.jpg'),
        new AssetImage('images/app_pics/hospitality.jpg'),
        new AssetImage('images/app_pics/mecheng.jpg'),
        new AssetImage('images/app_pics/travel.jpg'),
      ],
      animationCurve: Curves.fastOutSlowIn,
      animationDuration: Duration(seconds: 1),
    );

    Widget banner = new Padding(
      padding: const EdgeInsets.only(top: 20.0, left: 20.0),
      child: new Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5.0),
              bottomRight: Radius.circular(5.0)),
          //color: Colors.amber.withOpacity(0.5),
        ),
        padding: const EdgeInsets.all(10.0),
      ),
      // ),
      //  ),
    );

    return new Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(title: Text('College of Cape Town'),),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            Image.asset('images/drawer/banner3.jpg'),
            new ListTile(
                title: new Text("About us"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CardAboutPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Principal's Message"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CardCeoPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("SRC Council"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SRCCouncil()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("News and Events"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new FutureEvents()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Prospective Students"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ProScreen()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Current Students Login"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new Login3()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Social Media Links"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SampleMain()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Faqs"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new FAQS()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Central Office Location"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MapPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("College Campuses - Maps"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CampusMaps()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Privacy"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CardPrivacyPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Other Links"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new GridviewStud()));
                }
            ),
            new Divider(),

            new ListTile(
              title: new Text("Close"),
              trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
              onTap: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      ),
      body: new Center(
        child: new Container(
          padding: const EdgeInsets.all(20.0),
          height: screenHeight / 1,
          child: new ClipRRect(
            borderRadius: BorderRadius.circular(30.0),
            child: new Stack(
              children: [
                carousel,
                banner,
              ],
            ),
          ),
        ),
      ),
    );
  }

  dispose() {
    controller.dispose();
    super.dispose();
  }
}