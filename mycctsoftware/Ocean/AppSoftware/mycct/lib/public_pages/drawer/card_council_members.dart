


import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:mycct/json_folder/json_council_members.dart';


class CouncilCardJSONPage extends StatefulWidget {

  @override
  _CouncilCardJSONPageState createState() => _CouncilCardJSONPageState();
}

class _CouncilCardJSONPageState extends State<CouncilCardJSONPage> {

  List<Note> _notes = List<Note>();

  Future<List<Note>> fetchNotes() async {

    var url = 'http://197.242.92.154/rest/tvetcolleges/Council';

    var response = await http.get(url);

    var notes = List<Note>();

    if (response.statusCode == 200) {
      var notesJson = json.decode(response.body);
      for (var noteJson in notesJson) {
        notes.add(Note.fromJson(noteJson));
      }
    }
    return notes;
  }

  @override
  void initState() {
    fetchNotes().then((value) {
      setState(() {
        _notes.addAll(value);
      });
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('College Council Members',style: TextStyle(color: Colors.white),
          ),
        ),
        body: ListView.builder(
          itemBuilder: (context, index) {
            return Card(
              elevation: 10.0,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(_notes[index].membername,
                      style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold,fontSize: 20.0),
                    ),
                    Image.asset(_notes[index].urlLocation,
                    ),
                    Text(_notes[index].salutation,
                      style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0),
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: _notes.length,
        )
    );
  }
}


