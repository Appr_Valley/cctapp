
import 'package:flutter/material.dart';


class Privacy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Privacy',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Column buildButtonColumn(IconData icon, String label) {
      Color color = Theme.of(context).primaryColor;

      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    Widget textSection = Container(
      padding: const EdgeInsets.all(5.0),
      child: Text(
        '''At College of Cape Town, accessible from http://www.cct.edu.za/, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by College of Cape Town and how we use it.
If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us through email at info@cct.edu.za

Log Files

College of Cape Town follows a standard procedure of using log files. These files log visitors when they consume the mobile app. All hosting companies do this and a part of hosting services' analytics. The information collected by log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information.

Privacy Policies

You may consult this list to find the Privacy Policy for each of the advertising partners of College of Cape Town. Our Privacy Policy was created with the help of the Privacy Policy Generator.
Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on College of Cape Town, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.

Note that College of Cape Town has no access to or control over these cookies that are used by third-party advertisers.

Third Party Privacy Policies

College of Cape Town's Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options.
You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers' respective websites. What Are Cookies?

Children's Information

Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.
College of Cape Town does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.

Online Privacy Policy Only

This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in College of Cape Town. This policy is not applicable to any information collected offline or via channels other than this website.

Consent

By using our website, you hereby consent to our Privacy Policy and agree to its Terms and Conditions.
     ''',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Privacy statement',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Privacy Statement'),
          backgroundColor: Colors.red,
        ),
        body: ListView(
          children: [
            new Container(
              constraints: new BoxConstraints.expand(height: 130.0),
              padding:
              new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('images/drawer/banner3.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            //titleSection,
            //buttonSection,
            textSection,
          ],
        ),
      ),
    );
  }
}

