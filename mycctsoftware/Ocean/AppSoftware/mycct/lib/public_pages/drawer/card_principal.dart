

import 'package:flutter/material.dart';



class CardCeoPage extends StatefulWidget {

  @override
  _CardCeoPageState createState() => _CardCeoPageState();
}

class _CardCeoPageState extends State<CardCeoPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Principal Message',style: TextStyle(color: Colors.white),
          ),
        ),
        body: ListView.builder(
          itemBuilder: (context, index) {
            return Card(
              elevation: 10.0,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      constraints: new BoxConstraints.expand(height: 220.0,width: 300.0),
                      padding:
                      new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: new AssetImage('images/drawer/ceouse.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),

                    ),
                    Text(
                      ''' We stand ready to provide you with a range of quality vocational education and training programmes.

    The TVET College sector has a growing role to play in the provision of the intermediate to higher level skills required to support economic growth and development.

    The College of Cape Town has the resources and the infrastructure to respond to the education and training needs of our communities and employers.

    At the College of Cape Town, we work hard at creating an environment that is conducive to learning. Our facilities are easily accessible. We pride ourselves in having a competent and committed staff that go to great lengths to provide you with the necessary services.

    We have committed ourselves to provide you with Quality Education and Training. In this regard, we emphasize the provision of quality in terms of teaching, learning & services.

    We look forward to be of service to you and would like to be your preferred choice.


    Yours in Further Education and Training
    Louis van Niekerk, Principal       ''',
                      softWrap: true,
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: 1,
        )
    );
  }
}


