

import 'package:flutter/material.dart';

class CeoMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Principal Message',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Column buildButtonColumn(IconData icon, String label) {
      Color color = Theme.of(context).primaryColor;

      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    Widget textSection = Container(
      padding: const EdgeInsets.all(5.0),
      child: Text(
        ''' We stand ready to provide you with a range of quality vocational education and training programmes.

    The TVET College sector has a growing role to play in the provision of the intermediate to higher level skills required to support economic growth and development.

    The College of Cape Town has the resources and the infrastructure to respond to the education and training needs of our communities and employers.

    At the College of Cape Town, we work hard at creating an environment that is conducive to learning. Our facilities are easily accessible. We pride ourselves in having a competent and committed staff that go to great lengths to provide you with the necessary services.

    We have committed ourselves to provide you with Quality Education and Training. In this regard, we emphasize the provision of quality in terms of teaching, learning & services.

    We look forward to be of service to you and would like to be your preferred choice.


    Yours in Further Education and Training
    Louis van Niekerk, Principal       ''',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Message from Principal'),
          backgroundColor: Colors.red,
        ),
        body: ListView(
          children: [
            new Container(
              constraints: new BoxConstraints.expand(height: 250.0),
              padding:
              new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('images/drawer/ceouse.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            //titleSection,
            //buttonSection,
            textSection,
          ],
        ),
      ),
    );
  }
}
