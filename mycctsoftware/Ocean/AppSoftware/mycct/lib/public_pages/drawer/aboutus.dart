

import 'package:flutter/material.dart';
import 'package:mycct/styles/style.dart';

class Aboutus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'About us',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Column buildButtonColumn(IconData icon, String label) {
      Color color = Theme.of(context).primaryColor;

      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    Widget textSection = Container(
      padding: const EdgeInsets.all(5.0),
      child: Text(
        '''As a leading provider of Education and Training in mainly the Technical and Vocational Education and Training (TVET) band, the College of Cape Town has much to offer students and prospective partners as an alternative to General Education and Training.  Our courses lead to recognised, accredited qualifications that are in high demand by commerce and industry.
        
The College strives to provide high-quality education and training to help you equip yourself with the qualifications and skills you need to start out on a chosen career path.

Our College is situated in the central area of the Peninsula, and serves the greater Cape Town area, including a large percentage of traditionally disadvantaged areas and townships.  Although the majority of our students hail from the greater Cape Town metropolitan region, we proudly accept students from all other regions of South Africa, Namibia and other African countries, and many countries abroad.

All campuses have well-equipped workshops, lecture rooms, computer rooms, studios for practical work and well-equipped media centres.  Our seven campuses include:

         Athlone Campus
         City Campus
         Crawford Campus
         Guguletu Campus
         Pinelands Campus
         Thornton Campus
         Wynberg Campus
Our Central Office is located in Salt River, Cape Town.
 
The College of Cape Town also has 3 residences, all conveniently situated in close proximity to the respective campuses.  They are:

    City Residence (Ladies and Gents)
    Crawford Residence (Ladies and Gents)
    Thornton Residence (Ladies and Gents)
    
The Crawford, City and Thornton Campuses boast state of the art Open Learning Centers offering them the latest in technology.  Services include a library, computer and internet access, printing, faxing, photocopying and book-binding facilities.  We are in the process of extending the initiative to our other campuses and all students can make use of this service at any campus.  It is an initiative aimed at improving the quality of education and gives students a leading edge.

Student Support Services are available at all campuses and include services such as student counselling, career guidance, assistance with study skills, and peer education, to name but a few.
The College of Cape Town is committed to serving and educating learners from all communities, offering its learners a holistic learning experience in the vocational discipline of their choice.      ''',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('About us'),
          backgroundColor: Colors.red,
        ),
        body: ListView(
          children: [
            new Container(
              constraints: new BoxConstraints.expand(height: 130.0),
              padding:
              new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('images/drawer/banner3.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            //titleSection,
            //buttonSection,
            textSection,
          ],
        ),
      ),
    );
  }
}
