import 'package:flutter/material.dart';

class AboutUS extends StatefulWidget {
  @override
  _AboutUSState createState() => _AboutUSState();
}

class _AboutUSState extends State<AboutUS> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('About us ..'),
          backgroundColor: Colors.red,
        ),
        body: new ListView(
          children: <Widget>[
            new Container(
              constraints: new BoxConstraints.expand(height: 120.0),
              padding:
              new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('images/banner.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(10.0),
              child: new Text(
  '''As a leading provider of Education and Training in mainly the Technical and Vocational Education and Training (TVET) band, the College of Cape Town has much to offer students and prospective partners as an alternative to General Education and Training.  Our courses lead to recognised, accredited qualifications that are in high demand by commerce and industry. \n\nThe College strives to provide high-quality education and training to help you equip yourself with the qualifications and skills you need to start out on a chosen career path.\n\nOur College is situated in the central area of the Peninsula, and serves the greater Cape Town area, including a large percentage of traditionally disadvantaged areas and townships.\n\nAlthough the majority of our students hail from the greater Cape Town metropolitan region, we proudly accept students from all other regions of South Africa, Namibia and other African countries, and many countries abroad.\n\nAll campuses have well-equipped workshops, lecture rooms, computer rooms, studios for practical work and well-equipped media centres. 
  ''',
                style: new TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 15.0,
                ),
              ),
            )
          ],
        ));
  }
}


