import 'package:flutter/material.dart';

class FutureEvents extends StatefulWidget {
  @override
  _FutureEvents createState() => _FutureEvents();
}

class _FutureEvents extends State<FutureEvents> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('News and Events'),
          backgroundColor: Colors.red,
        ),
        body: new ListView(
          children: <Widget>[
            new Container(
              constraints: new BoxConstraints.expand(height: 120.0),
              padding:
              new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('images/banner.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Image.asset(
              'images/grad.jpg',
            ),
            new Divider(),
            Image.asset(
              'images/sevent2.jpg',
            ),
            new Divider(),
            Image.asset(
              'images/sevent3.jpg',
            ),
            new Divider(),
            Image.asset(
              'images/sevent4.jpg',
            ),
            new Divider(),
            Image.asset(
              'images/sevent5.jpg',
            ),
          ],
        ));
  }
}


