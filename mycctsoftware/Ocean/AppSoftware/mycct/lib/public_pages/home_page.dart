
import 'package:flutter/material.dart';
import 'package:mycct/public_pages/drawer/card_principal.dart';
import 'package:mycct/public_pages/drawer/card_about.dart';
import 'package:mycct/public_pages/drawer/card_privacy.dart';
import 'package:mycct/progress_pages/progress_faqs.dart';
import 'package:mycct/social_media/sample_social_page.dart';
import 'package:mycct/local_menus/listview_menu_page.dart';
import 'package:mycct/gridview/gridview_student.dart';
import 'package:mycct/login/abs_login_page.dart';
import 'package:mycct/progress_pages/campus_maps.dart';
import 'package:mycct/public_pages/future_events.dart';
import 'package:mycct/public_pages/map.dart';
import 'package:mycct/progress_pages/progress_src_council.dart';
import 'package:mycct/notes_menus/sample_menu.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            Image.asset('images/drawer/banner3.jpg'),
            new ListTile(
                title: new Text("About us"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CardAboutPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Principal's Message"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CardCeoPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("SRC Council"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SRCCouncil()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("News and Events"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new FutureEvents()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Prospective Students"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ListMenu()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Current Student Login"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new Login3()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Social Media Links"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SampleMain()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Faqs"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new FAQS()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Central Office Location"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MapPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("College Campuses - Maps"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CampusMaps()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Privacy"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new CardPrivacyPage()));
                }
            ),
            new Divider(),
            new ListTile(
                title: new Text("Other Links too"),
                trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new GridviewStud()));
                }
            ),
            new Divider(),
            new ListTile(
              title: new Text("Close"),
              trailing: new Icon(Icons.arrow_right,color: Colors.redAccent),
              onTap: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      ),
       body: DefaultTabController(
        length: 6,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                title: new Text('College of Cape Town'),
                  expandedHeight: 500.0,
                  floating: false,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    //centerTitle: true,
                    background:
                    new Image.asset('images/app_pics/master.jpg',fit: BoxFit.cover),
                  )
              ),
            ];
          },
          body: ListView(
            children: [
              Image.asset(
                'images/app_pics/artanddesign.jpg',
              ),
              new Divider(),
              Image.asset(
                'images/app_pics/beauty.jpg',
              ),
              new Container(
                  constraints: new BoxConstraints.expand(
                      height: 500.0
                  ),
                  padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage('images/app_pics/building.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
              ),
              new Divider(),
              Image.asset(
                'images/app_pics/education.jpg',
              ),
              new Container(
                  constraints: new BoxConstraints.expand(
                      height: 500.0
                  ),
                  padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage('images/app_pics/travel.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
              ),
              new Container(
                  constraints: new BoxConstraints.expand(
                      height: 500.0
                  ),
                  padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage('images/app_pics/electrical.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
              ),
              new Divider(),
              new Container(
                  constraints: new BoxConstraints.expand(
                      height: 500.0
                  ),
                  padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage('images/app_pics/it.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
              ),
              new Divider(),
              new Container(
                constraints: new BoxConstraints.expand(
                    height: 500.0
                ),
                padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage('images/app_pics/busstudies.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              new Divider(),
              new Container(
                constraints: new BoxConstraints.expand(
                    height: 500.0
                ),
                padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage('images/app_pics/hospitality.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),

        ),
      ),
    );
  }
}


