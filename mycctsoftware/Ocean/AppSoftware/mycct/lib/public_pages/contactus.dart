import 'package:flutter/material.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUSState createState() => _ContactUSState();
}

class _ContactUSState extends State<ContactUs> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Contact us ..'),
          backgroundColor: Colors.red,
        ),
        body: new ListView(
          children: <Widget>[
            new Container(
              constraints: new BoxConstraints.expand(height: 120.0),
              padding:
              new EdgeInsets.only(left: 0.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('images/banner.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(5.0),
              child: new Text(
  '''
  Central Office
  334 Albert Road
  Salt River
  Tel: +27 21 404 6700
  Fax: +27 21 404 6701
           
  Postal Address
  PO Box 1054
  Cape Town
  8000
  Republic of South Africa
  
  Info Centre
  Tel (South Africa only): 086 010 3682
            
  Fax (South Africa only): +27 86 615 0582
            
  ''',
                style: new TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
            )
          ],
        ));
  }
}



