import 'package:flutter/material.dart';
import 'package:mycct/main.dart';
import 'package:mycct/utils/prospective_notes.dart';
import 'package:mycct/progress_pages/public_holidays.dart';
import 'package:mycct/progress_pages/academic_calendar.dart';
import 'package:mycct/progress_pages/aca_calendar.dart';

class ProScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProScreenState();
  }

}

class ProScreenState extends State<ProScreen>{

  List<Note> _notes;

  ProScreenState(){
    _notes = Notes.initializeNotes().getNotes;
  }


  _handleIconDisplay(int index){
    bool readStatus = _notes[index].getReadState;
    return Icon((readStatus? Icons.arrow_right:Icons.arrow_right),color: (readStatus)?Colors.green:Colors.red,);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Prospective Student Menu '),
        backgroundColor: Colors.deepOrange,
      ),
      body: ListView.builder(
          itemCount: _notes.length,
          itemBuilder: (context,index){
            return Container(
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color:Colors.grey,width: 1.0))
              ),
              child: ListTile(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(_notes[index].getTitle),
                    _handleIconDisplay(index)
                  ],
                ),
                onTap: () async {

                  String grade = (_notes[index].getTitle);

                  switch (grade) {

                    case 'College and Public Holidays':
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new Holidays()));
                      break;

                    case 'Academic Calendar':
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new AcaCal()));
                      break;

                    case 'Exit Menu':
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MyApp()));
                      break;

                    default:
                      print("Invalid Grade");
                  }


                  //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ContactUs()));

                },
              ),
            );
          }),
    );
  }

}