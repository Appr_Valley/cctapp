import 'package:flutter/material.dart';
import 'package:mycct/utils/staff_notes.dart';
import 'package:mycct/messaging/mess_main.dart';
import 'package:mycct/main.dart';


class AdminScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AdminScreenState();
  }

}

class AdminScreenState extends State<AdminScreen>{

  List<Note> _notes;

  AdminScreenState(){
    _notes = Notes.initializeNotes().getNotes;
  }


  _handleIconDisplay(int index){
    bool readStatus = _notes[index].getReadState;
    return Icon((readStatus? Icons.arrow_right:Icons.arrow_right),color: (readStatus)?Colors.green:Colors.red,);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Staff Menu'),
        backgroundColor: Colors.deepOrange,
      ),
      body: ListView.builder(
          itemCount: _notes.length,
          itemBuilder: (context,index){
            return Container(
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color:Colors.grey,width: 1.0))
              ),
              child: ListTile(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(_notes[index].getTitle),
                    _handleIconDisplay(index)
                  ],
                ),
                onTap: () async {

                  String grade = (_notes[index].getTitle);

                  switch (grade) {

                    case 'Send Push Notifications to students':
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MessMainPage()));
                      break;

                    case 'Exit Staff Menu':
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MyApp()));
                      break;


                    default:
                      print("Invalid Grade");
                  }
                },
              ),
            );
          }),
    );
  }

}