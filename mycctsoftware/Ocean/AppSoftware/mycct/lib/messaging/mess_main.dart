
import 'package:flutter/material.dart';
import 'package:mycct/messaging/messaging_widget.dart';


class MessMainPage extends StatelessWidget {
  //final String appTitle;

  //const MainPage({this.appTitle});

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text('Send Push Notifications'),
    ),
    body: MessagingWidget(),
  );
}
