

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:mycct/student_pages/student_main_page.dart';
import 'package:mycct/local_menus/admin_menu.dart';






class LoginScreen1 extends StatefulWidget {
  @override
  _LoginScreen1State createState() => _LoginScreen1State();
}

class _LoginScreen1State extends State<LoginScreen1> {



  MyPreferences _myPreferences = MyPreferences();


  int username=0;
  String password='';
  String first_names='';
  String surname = '';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String tid = '';


  final FirebaseMessaging _messaging = FirebaseMessaging();
  //@override

  String msg='';

  TextEditingController user=new TextEditingController();
  TextEditingController pass=new TextEditingController();

  Future<List> _login() async {
    final response = await http.post("http://197.242.92.154/rest/tvetcolleges/Login", body: {
      "username": user.text,
      "password": pass.text,
    });

    var dataUser = json.decode(response.body);
    print(dataUser);

    _myPreferences.firstnames = dataUser['first_names'];
    _myPreferences.surname = dataUser['surname'];
    _myPreferences.college_id = dataUser['college_id'];
    _myPreferences.campusno = dataUser['campusno'];
    _myPreferences.usertypeno = dataUser['usertypeno'];
    _myPreferences.mobileno = dataUser['mobileno'];
    _myPreferences.commit();

    if(dataUser.length==0){
      setState(() {
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text('Incorrect username'),
                content: Text('Try with a different username'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            }
        );
        return;
      });
    }else{

      http.post("http://197.242.92.154/rest/tvetcolleges/TUpdate", body: {
        "username": user.text,
        "tokenid": tid,
      });

      if(dataUser['usertypeno']==1){

        Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new StudMainPage()));
        //Navigator.of(context).push(MaterialPageRoute(builder: (context) => SuperV()));
        //Navigator.of(context).pop();

      }else if(dataUser['usertypeno']==2){

        Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new AdminMenu()));
        //Navigator.of(context).push(MaterialPageRoute(builder: (context) => SettingsPage()));
        //Navigator.of(context).pop();
      }
    }
    return dataUser;

  }


  Widget build(BuildContext context) {


    return new Container(

        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new ClipPath(
                clipper: MyClipper(),
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: ExactAssetImage('images/grad.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(top: 160.0, bottom: 50.0),
                  child: Column(
                    children: <Widget>[

                    ],
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.5),
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                margin:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                child: Row(
                  children: <Widget>[
                    new Padding(
                      padding:
                      EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                      child: Icon(
                        Icons.person_outline,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 20.0,
                      width: 1.0,
                      color: Colors.grey.withOpacity(0.5),
                      margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                    ),
                    new Expanded(
                      child: TextField(
                        controller: user,
                        decoration: InputDecoration(
                          labelText: 'STUDENTNO',
                          labelStyle: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              color: Colors.grey),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.5),
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                margin:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Row(
                  children: <Widget>[
                    new Padding(
                      padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                      child: Icon(
                        Icons.security,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30.0,
                      width: 1.0,
                      color: Colors.grey.withOpacity(0.5),
                      margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                    ),
                    new Expanded(
                      child: TextField(
                        controller: pass,
                        decoration: InputDecoration(
                          //hintText: 'Username',
                          labelText: 'IDNO',
                          labelStyle: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              color: Colors.grey),
                        ),
                        obscureText: true,
                      ),
                    )
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 60.0,
                    width: 300,

                    child: Material(
                      borderRadius: BorderRadius.circular(40.0),
                      shadowColor: Colors.grey,
                      color: Colors.pink,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {

                          _login();
                          _myPreferences.username = user.toString();
                          _myPreferences.password = pass.text;
                          _myPreferences.commit();
                          //Navigator.pop(context);
                          //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MessMainPage()));
                        },
                        child: Center(
                          child: Text(
                            'Login',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        )
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.85);
    p.arcToPoint(
      Offset(0.0, size.height * 0.85),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}


