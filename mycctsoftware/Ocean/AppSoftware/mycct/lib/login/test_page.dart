import 'package:flutter/material.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';

MyPreferences _myPreferences = MyPreferences();

@override
void initState() {
  //super.initState();
  _myPreferences.init().then((value) {
  });
}


class Second extends StatelessWidget {
  @override



  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Hi ' + _myPreferences.firstnames),),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Text('This is the SECOND screen'),
              new RaisedButton(
                  child:  new Text('Next'),
                  //onPressed: (){Navigator.of(context).pushNamed('/Third');}
              )
            ],
          ),
        ),
      ),
    );
  }
}