import 'package:flutter/material.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:mycct/main.dart';
import 'package:mycct/messaging/mess_main.dart';



class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SettingsState();
  }
}

class SettingsState extends State<SettingsPage> {
  MyPreferences _myPreferences = MyPreferences();

  int username=0;
  String password='';
  String first_names='';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String tid = '';

  String msg='';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Admin Page for - " + _myPreferences.firstnames),backgroundColor: Colors.redAccent,
        ),
        body: Padding(
          padding: EdgeInsets.all(15),
          child: ListView(
            children: <Widget>[
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50.0,
                    width: 280,
                    child: Material(
                      borderRadius: BorderRadius.circular(30.0),
                      shadowColor: Colors.grey,
                      color: Colors.redAccent,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MessMainPage()));
                        },
                        child: Center(
                          child: Text(
                            'Send Push Notifications',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    ),

                  ),
                ],
              ),
              SizedBox(height: 5.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50.0,
                    width: 280,
                    child: Material(
                      borderRadius: BorderRadius.circular(30.0),
                      shadowColor: Colors.grey,
                      color: Colors.redAccent,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new GListPage()));
                        },
                        child: Center(
                          child: Text(
                            'Student List',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    ),

                  ),
                  SizedBox(height: 10.0),
                ],
              ),
              SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50.0,
                    width: 280,
                    child: Material(
                      borderRadius: BorderRadius.circular(30.0),
                      shadowColor: Colors.grey,
                      color: Colors.redAccent,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyApp()));
                          //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new LoginScreen1()));
                        },
                        child: Center(
                          child: Text(
                            'EXIT',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });

  }
}
