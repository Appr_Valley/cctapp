import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:mycct/main.dart';
import 'package:mycct/student_pages/student_welcome_page.dart';
import 'package:mycct/notes_menus/admin_menu.dart';

class Urls {
  static const BASE_API_URL = "http://197.242.92.154/rest/tvetcolleges";
}

class Login3 extends StatefulWidget {

  @override
  _Login3State createState() => _Login3State();
}

class _Login3State extends State<Login3> {

  MyPreferences _myPreferences = MyPreferences();


  int username=0;
  String password='';
  String first_names='';
  String surname='';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String mobileno='';
  String tid = '';

  var users = '';

  //MyPreferences _myPreferences = MyPreferences();


  final FirebaseMessaging _messaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });

    _messaging.getToken().then((token) {
      print(token);
      tid = token;
    });

  }


  //bool _isLoading = false;
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  //Login Code

  String msg='';

  Future<List> _login() async {
    final response = await http.post("http://197.242.92.154/rest/tvetcolleges/Login", body: {
      "username": _usernameController.text,
      "password": _passwordController.text,
    });

    var dataUser = json.decode(response.body);
    print(dataUser);


    _myPreferences.firstnames = dataUser['first_names'];
    _myPreferences.surname = dataUser['surname'];
    _myPreferences.college_id = dataUser['college_id'];
    _myPreferences.campusno = dataUser['campusno'];
    _myPreferences.usertypeno = dataUser['usertypeno'];
    _myPreferences.commit();


    if(dataUser.length==0){
      setState(() {
        msg="Login Fail";
      });
    }else{

      http.post("http://197.242.92.154/rest/tvetcolleges/TUpdate", body: {
        "username": _usernameController.text,
        "tokenid": tid,
      });

      if(dataUser['usertypeno']==1){

        //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SuperV()));
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => GuestWelcomePage()));
        //Navigator.of(context).pop();
      }else if(dataUser['usertypeno']==2){

        Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminScreen()));
      }
    }
    return dataUser;
  }

  @override

  Widget build(BuildContext context) {
    return new Scaffold(
      //appBar: new AppBar(title: new Text('Hi ' + _myPreferences.firstnames),),
      body: new Container(

          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new ClipPath(
                  clipper: MyClipper(),
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: ExactAssetImage('images/grad.jpg'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 160.0, bottom: 50.0),
                    child: Column(
                      children: <Widget>[

                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.5),
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  margin:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                  child: Row(
                    children: <Widget>[
                      new Padding(
                        padding:
                        EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                        child: Icon(
                          Icons.person_outline,
                          color: Colors.grey,
                        ),
                      ),
                      Container(
                        height: 15.0,
                        width: 1.0,
                        color: Colors.grey.withOpacity(0.5),
                        margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                      ),
                      new Expanded(
                        child: TextField(
                          controller: _usernameController,
                          decoration: InputDecoration(
                            labelText: 'STUDENTNO',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                color: Colors.grey),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.5),
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                  child: Row(
                    children: <Widget>[
                      new Padding(
                        padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                        child: Icon(
                          Icons.security,
                          color: Colors.grey,
                        ),
                      ),
                      Container(
                        height: 20.0,
                        width: 1.0,
                        color: Colors.grey.withOpacity(0.5),
                        margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                      ),
                      new Expanded(
                        child: TextField(
                          controller: _passwordController,
                          decoration: InputDecoration(
                            //hintText: 'Username',
                            labelText: 'IDNO/PASSPORTNO',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                color: Colors.grey),
                          ),
                          obscureText: true,
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        height: 50.0,
                        width: 300,

                        child: SizedBox(

                          height: 50,
                          width: double.infinity,

                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(50.0)),
                            color: Colors.redAccent,
                            elevation: 7.0,

                            child: Text(
                              'Log in',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              //Login Code Here
                              _login();
                              _myPreferences.username = _usernameController.text;
                              _myPreferences.password = _passwordController.text;
                              _myPreferences.commit();
                            },
                          ),
                        )
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        height: 50.0,
                        width: 300,

                        child: SizedBox(

                          height: 50,
                          width: double.infinity,

                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(50.0)),
                            color: Colors.redAccent,
                            elevation: 7.0,

                            child: Text(
                              'Exit',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MyApp()));
                            },
                          ),
                        )
                    ),


                  ],
                ),
              ],
            ),
          )
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.85);
    p.arcToPoint(
      Offset(0.0, size.height * 0.85),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
