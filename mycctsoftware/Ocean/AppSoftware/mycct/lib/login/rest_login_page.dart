
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:mycct/login/admin_main_page.dart';
import 'package:http/http.dart' as http;
import 'package:mycct/student_pages/student_main_page.dart';



class Urls {
  static const BASE_API_URL = "http://197.242.92.154/rest/tvetcolleges";
}


class RLogin extends StatefulWidget {
  @override
  _RLoginState createState() => _RLoginState();
}

class _RLoginState extends State<RLogin> {

  MyPreferences _myPreferences = MyPreferences();


  int username=0;
  String password='';
  String first_names='';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String tid = '';

  var users = '';

  //MyPreferences _myPreferences = MyPreferences();


  final FirebaseMessaging _messaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });

    _messaging.getToken().then((token) {
      print(token);
      tid = token;
    });

  }




  bool _isLoading = false;
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  //Login Code

  String msg='';

  Future<List> _login() async {
    final response = await http.post("http://197.242.92.154/rest/tvetcolleges/Login", body: {
      "username": _usernameController.text,
      "password": _passwordController.text,
    });

    var dataUser = json.decode(response.body);
    print(dataUser);


    _myPreferences.firstnames = dataUser['first_names'];
    //_myPreferences.username = dataUser['username'];
    _myPreferences.college_id = dataUser['college_id'];
    _myPreferences.campusno = dataUser['campusno'];
    _myPreferences.usertypeno = dataUser['usertypeno'];
    _myPreferences.commit();


    if(dataUser.length==0){
      setState(() {
        msg="Login Fail";
      });
    }else{

      http.post("http://197.242.92.154/rest/tvetcolleges/TUpdate", body: {
        "username": _usernameController.text,
        "tokenid": tid,
      });

      if(dataUser['usertypeno']==1){

        //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SuperV()));
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => StudMainPage()));
        //Navigator.of(context).pop();

      }else if(dataUser['usertypeno']==2){

        //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SettingsPage()));
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => SettingsPage()));
        //Navigator.of(context).pop();
      }
    }
    return dataUser;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Log in'),),

      body: Padding(


        padding: const EdgeInsets.all(32.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    hintText: 'Username'
                ),
                controller: _usernameController,
              ),
              Container(height: 20,),
              TextField(
                decoration: InputDecoration(
                    hintText: 'Password'
                ),
                controller: _passwordController,
              ),
              Container(height: 20,),
              _isLoading ? CircularProgressIndicator() : SizedBox(
                height: 50,
                width: double.infinity,
                child: RaisedButton(
                  color: Colors.redAccent,
                  elevation: 7.0,
                  child: Text(
                    'Log in',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    //Login Code Here
                    _login();
                    _myPreferences.username = _usernameController.toString();
                    _myPreferences.password = _passwordController.text;
                    _myPreferences.commit();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}







