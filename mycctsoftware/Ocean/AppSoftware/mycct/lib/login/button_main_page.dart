
import 'package:flutter/material.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:mycct/messaging/mess_main.dart';
import 'package:mycct/main.dart';



class ButtonMainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ButtonMainState();
  }
}

class ButtonMainState extends State<ButtonMainPage> {
  MyPreferences _myPreferences = MyPreferences();

  int username=0;
  String password='';
  String first_names='';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String tid = '';

  String msg='';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Admin Page for - " + _myPreferences.firstnames),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
              children: <Widget>[
                SizedBox(
                  width: 200.0,
                  height: 50.0,
                  child: new RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0)),
                    color: Colors.redAccent,
                    elevation: 7.0,
                    child: new Text(
                      'Send Push Notifications',
                      style: TextStyle(color: Colors.white,),
                    ),

                    onPressed: () {
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MessMainPage()));
                    },
                  ),
                ),
                const SizedBox(height: 30),
                SizedBox(
                  width: 200.0,
                  height: 50.0,
                  child: new RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0)),
                    color: Colors.redAccent,
                    elevation: 7.0,
                    child: new Text(
                      'Exit Menu',
                      style: TextStyle(color: Colors.white,),
                    ),

                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new MyApp()));
                    },
                  ),
                ),
              ]
          ),
        )
    );

  }

  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });

  }
}
