
class Notes {
  List<Note> _notes;

  Notes.initializeNotes () {
    _notes = List<Note>();
    _notes.add(Note('College and Public Holidays', 'This is some note regarding Wedding',false));
    _notes.add(Note('Academic Calendar', 'This is some note regarding Office',false));
    _notes.add(Note('Exit Menu', 'This is some note regarding Birthday',false));

  }

  List<Note> get getNotes => _notes;
}


class Note {
  String _title;
  String _noteContent;
  bool _noteRead;

  Note(this._title,this._noteContent,this._noteRead);

  String get getTitle => _title;
  String get getNoteContent => _noteContent;
  bool get getReadState => _noteRead;
  set setReadState(bool readState) => this._noteRead = readState;
}

