
class Notes {
  List<Note> _notes;

  Notes.initializeNotes () {
    _notes = List<Note>();
    _notes.add(Note('My Baseline Data', 'This is some note regarding Wedding',false));
    _notes.add(Note('Student Menu', 'This is some note regarding Office',false));
    _notes.add(Note('Public and College Holidays', 'This is some note regarding Office',false));
    _notes.add(Note('College Academic Calendar', 'This is some note regarding Office',false));
    _notes.add(Note('Exit Student Menu', 'This is some note regarding Birthday',false));

  }

  List<Note> get getNotes => _notes;
}


class Note {
  String _title;
  String _noteContent;
  bool _noteRead;

  Note(this._title,this._noteContent,this._noteRead);

  String get getTitle => _title;
  String get getNoteContent => _noteContent;
  bool get getReadState => _noteRead;
  set setReadState(bool readState) => this._noteRead = readState;
}