
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';






class SocialMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.white,
        appBar: new AppBar(
            title: new Text("Social Media Links"), backgroundColor: Colors.redAccent),
        body: Column(children: <Widget>[

          Row(
            //ROW 1
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(5.0),
                child: new RawMaterialButton(
                  onPressed: () async {
                    if (await canLaunch("http://www.cct.edu.za/")) {
                    await launch("http://www.cct.edu.za/");
                    }
                  },
                  child: new Icon(FontAwesomeIcons.chrome,
                    color: Colors.redAccent,
                    size: 50.0,
                  ),
                  
                  shape: new CircleBorder(),
                  elevation: 2.0,
                  fillColor: Colors.white,
                  padding: const EdgeInsets.all(7.0),
                ),
              ),
              Container(
                padding: EdgeInsets.all(7.0),
                child: new RawMaterialButton(
                  onPressed: () async {
                    if (await canLaunch("https://af-za.facebook.com/CCT.Home/")) {
                      await launch("https://af-za.facebook.com/CCT.Home/");
                    }
                  },
                  child: new Icon(FontAwesomeIcons.facebook,
                    color: Colors.blue,
                    size: 50.0,
                  ),
                  shape: new CircleBorder(),
                  elevation: 2.0,
                  fillColor: Colors.white,
                  padding: const EdgeInsets.all(7.0),
                ),
              ),
              Container(
                padding: EdgeInsets.all(7.0),
                child: new RawMaterialButton(
                  onPressed: () async {
                    if (await canLaunch("https://twitter.com/CCT_Official/")) {
                      await launch("https://twitter.com/CCT_Official/");
                    }
                  },
                  child: new Icon(FontAwesomeIcons.twitter,
                    color: Colors.redAccent,
                    size: 50.0,
                  ),
                  shape: new CircleBorder(),
                  elevation: 2.0,
                  fillColor: Colors.white,
                  padding: const EdgeInsets.all(7.0),
                ),
              ),
           ],
          ),
          Row(//ROW 2
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(7.0),
                  child: new RawMaterialButton(
                    onPressed: () {
                      //Navigator.pushReplacementNamed(context, '/pages/listarUsuarios');
                    },
                    child: new Icon(
                      Icons.settings,
                      color: Colors.redAccent,
                      size: 50.0,
                    ),
                    shape: new CircleBorder(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    padding: const EdgeInsets.all(7.0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(7.0),
                  child: new RawMaterialButton(
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/LoginPage');
                    },
                    child: new Icon(
                      Icons.backup,
                      color: Colors.lightBlue[300],
                      size: 50.0,
                    ),
                    shape: new CircleBorder(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    padding: const EdgeInsets.all(7.0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(7.0),
                  child: new RawMaterialButton(
                    onPressed: () {
                      //Navigator.pushReplacementNamed(context, '/LoginPage');
                    },
                    child: new Icon(
                      Icons.alarm_on,
                      color: Colors.yellowAccent,
                      size: 50.0,
                    ),
                    shape: new CircleBorder(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    padding: const EdgeInsets.all(7.0),
                  ),
                ),
              ]),
          Row(// ROW 3
              children: [
                Container(
                  padding: EdgeInsets.all(7.0),
                  child: new RawMaterialButton(
                    onPressed: () {
                      //Navigator.pushReplacementNamed(context, '/pages/listarUsuarios');
                    },
                    child: new Icon(FontAwesomeIcons.facebook),
                    shape: new CircleBorder(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    padding: const EdgeInsets.all(7.0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(7.0),
                  child: new RawMaterialButton(
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/LoginPage');
                    },
                    child: new Icon(
                      Icons.list,
                      color: Colors.grey[300],
                      size: 50.0,
                    ),
                    shape: new CircleBorder(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    padding: const EdgeInsets.all(7.0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(7.0),
                  child: new RawMaterialButton(
                    onPressed: () {
                      //Navigator.pushReplacementNamed(context, '/LoginPage');
                    },
                    child: new Icon(
                      Icons.cloud_done,
                      color: Colors.greenAccent,
                      size: 50.0,
                    ),
                    shape: new CircleBorder(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    padding: const EdgeInsets.all(7.0),
                  ),
                ),
              ]),
          //Row 4
          Row(
            children: <Widget>[
               Container(
                padding: EdgeInsets.all(7.0),
                child: new RawMaterialButton(
                  onPressed: () {
                    //Navigator.pushReplacementNamed(context, '/adminPage');
                  },
                  child: new Icon(
                    Icons.vpn_key,
                    color: Colors.red[900],
                    size: 50.0,
                  ),
                  shape: new CircleBorder(),
                  elevation: 2.0,
                  fillColor: Colors.transparent,
                  padding: const EdgeInsets.all(7.0),
                ),
              ),
            ],
          )
        ]));
  }
}


