
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';


class SampleMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.white,
        appBar: new AppBar(
            title: new Text("College Social Media Links"), backgroundColor: Colors.redAccent),
        body: Column(children: <Widget>[

          new Container(
            constraints: new BoxConstraints.expand(height: 300.0),
            padding:
            new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage('images/social.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),

          Row(
            //ROW 1
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [

              Container(
                padding: EdgeInsets.all(5.0),
                child: new RawMaterialButton(
                  onPressed: () async {
                    if (await canLaunch("http://www.cct.edu.za/")) {
                      await launch("http://www.cct.edu.za/");
                    }
                },
                  child: new Icon(FontAwesomeIcons.chrome,
                    color: Colors.redAccent,
                    size: 50.0,
                ),
                  shape: new CircleBorder(),
                  elevation: 2.0,
                  fillColor: Colors.white,
                  padding: const EdgeInsets.all(7.0),
                ),
             ),
              Container(
                padding: EdgeInsets.all(7.0),
                child: new RawMaterialButton(
                  onPressed: () async {
                    if (await canLaunch("https://af-za.facebook.com/CCT.Home/")) {
                      await launch("https://af-za.facebook.com/CCT.Home/");
                    }
                  },
                  child: new Icon(FontAwesomeIcons.facebook,
                    color: Colors.blue,
                    size: 50.0,
                  ),
                  shape: new CircleBorder(),
                  elevation: 2.0,
                  fillColor: Colors.white,
                  padding: const EdgeInsets.all(7.0),
                ),
              ),
              Container(
                padding: EdgeInsets.all(7.0),
                child: new RaisedButton(
                  onPressed: () async {
                    if (await canLaunch("https://twitter.com/CCT_Official/")) {
                      await launch("https://twitter.com/CCT_Official/");
                    }
                  },
                child: new Icon(FontAwesomeIcons.twitter,
                    color: Colors.redAccent,
                    size: 50.0,
                  ),

                  shape: new CircleBorder(),
                  elevation: 2.0,
                  //fillColor: Colors.white,
                  padding: const EdgeInsets.all(7.0),
                ),

              ),
            ],
          ),
        ]));
  }
}


