

import 'dart:convert';

import 'package:http/http.dart' as http;


class Urls {
  static const BASE_API_URL = "http://197.242.92.154/rest/tvetcolleges";
}

class ApiService {


  static Future<dynamic> _get(String url) async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        return null;
      }
    } catch (ex) {
      return null;
    }
  }

  static Future<List<dynamic>> getMenu() async {
    return await _get('local_json/main_menu.json');
  }


  static Future<List<dynamic>> getCampuses() async {
    return await _get('${Urls.BASE_API_URL}/Campuses');
  }

  static Future<List<dynamic>> getFAQS() async {
    return await _get('${Urls.BASE_API_URL}/Faqs');
  }

  static Future<dynamic> getSRCCouncil() async {
    return await _get('${Urls.BASE_API_URL}/SRCCouncil');
  }

  static Future<dynamic> getHolidays() async {
    return await _get('${Urls.BASE_API_URL}/Holidays');
  }

  static Future<dynamic> getAcaCal() async {
    return await _get('${Urls.BASE_API_URL}/Calendar');
  }


  static Future<dynamic> getCouncil() async {
    return await _get('${Urls.BASE_API_URL}/Council');
  }


  static Future<dynamic> getStudProfile() async {
    return await _get('${Urls.BASE_API_URL}/StudGet');
  }

}


