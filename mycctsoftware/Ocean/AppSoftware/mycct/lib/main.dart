import 'package:flutter/material.dart';
import 'package:mycct/public_pages/map.dart';
import 'package:mycct/public_pages/new_home_page.dart';
import 'package:mycct/notes_menus/sample_menu.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {

    return MyAppState(

    );

  }
}
class MyAppState extends State<MyApp> {

  int _selectedTab = 0;
  final _pageOptions = [
    HomePage(),
    MapPage(),
    HomeScreen()
  ];


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.red,
        primaryColor: const Color(0xFFf44336),
        accentColor: const Color(0xFFf44336),
        canvasColor: const Color(0xFFfafafa),
      ),


      home: Scaffold(

        body: _pageOptions[_selectedTab],

        bottomNavigationBar: BottomNavigationBar
          (
          currentIndex: _selectedTab,
          onTap: (int index) {
            setState(() {
              _selectedTab = index;
            });
          },

          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.star, color: Colors.redAccent),
              title: Text('Home', style: TextStyle(
                  color: Colors.grey
              )),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.map, color: Colors.redAccent),
              title: Text('Map', style: TextStyle(
                  color: Colors.grey
              )),
            ),
          ],
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }}





