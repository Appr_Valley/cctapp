import 'package:flutter/material.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:mycct/notes_menus/student_menu.dart';




class GuestWelcomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new GuestWelcomeState();
  }
}

class GuestWelcomeState extends State<GuestWelcomePage> {
  MyPreferences _myPreferences = MyPreferences();

  int username=0;
  String password='';
  String first_names='';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String tid = '';

  String msg='';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Welcome - " + _myPreferences.firstnames),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
              children: <Widget>[
                Card(
                  elevation: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          constraints: new BoxConstraints.expand(height: 220.0,width: 300.0),
                          padding:
                          new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                          decoration: new BoxDecoration(
                            image: new DecorationImage(
                              image: new AssetImage('images/drawer/ceo.jpg'),
                              fit: BoxFit.cover,
                            ),
                          ),

                        ),
                        Text(
                          ''' 
                    
 We are pleased to present this student mobile app to provide you the student with relevant information on demand.
 
 It wil evolve with more features to make your experience a pleasurable one.
 
 Please do not hesitate to let us know what you would like to see included in this app.
                    
Yours in Further Education and Training
Louis van Niekerk, Principal 
      ''',
                          softWrap: true,
                        ),
                      ],
                    ),
                  ),
                ),

                const SizedBox(height: 30),

                SizedBox(
                  width: 200.0,
                  height: 50.0,
                  child: new RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0)),
                    color: Colors.redAccent,
                    elevation: 7.0,
                    child: new Text(
                      'Proceed to Student Menu',
                      style: TextStyle(color: Colors.white,),
                    ),

                    onPressed: () {
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new StudentScreen()));
                    },
                  ),
                ),
              ]
          ),
        )
    );

  }

  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });

  }
}
