
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:http/http.dart' as http;
import 'package:mycct/student_pages/student_main_page.dart';



class Urls {
  static const BASE_API_URL = "http://197.242.92.154/rest/tvetcolleges";
}


class RLogin extends StatefulWidget {
  @override
  _RLoginState createState() => _RLoginState();
}

class _RLoginState extends State<RLogin> {

  MyPreferences _myPreferences = MyPreferences();


  int username=0;
  String password='';
  String first_names='';
  String surname = '';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String tid = '';

  var users = '';


  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });
  }




  bool _isLoading = false;
  //TextEditingController _usernameController = new TextEditingController();
  //TextEditingController _passwordController = new TextEditingController();

  //Login Code

  String msg='';

  Future<List> _login() async {
    final response = await http.post("http://197.242.92.154/rest/tvetcolleges/StudGet", body: {
      "studentno": _myPreferences.username,

    });

    var dataUser = json.decode(response.body);
    print(dataUser);


    _myPreferences.firstnames = dataUser['first_names'];
    _myPreferences.surname = dataUser['surname'];
    _myPreferences.college_id = dataUser['college_id'];
    _myPreferences.campusno = dataUser['campusno'];
    _myPreferences.usertypeno = dataUser['usertypeno'];
    _myPreferences.commit();


    if(dataUser.length==0){
      setState(() {
        msg="Login Fail";
      });
    }else{


      if(dataUser['usertypeno']==1){

        //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SuperV()));
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => StudMainPage()));
        //Navigator.of(context).pop();
      }
    }
    return dataUser;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:
              Text("Hi " + _myPreferences.firstnames),backgroundColor: Colors.redAccent,),
      body: Padding(

        padding: const EdgeInsets.all(32.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Container(height: 20,),
              _isLoading ? CircularProgressIndicator() : SizedBox(
                height: 50,
                width: double.infinity,
                child: RaisedButton(
                  color: Colors.redAccent,
                  elevation: 7.0,
                  child: Text(
                    'CLick for your Profile',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    //Login Code Here
                    _login();
                  },
                ),
              )

            ],
          ),
        ),
      ),
    );
  }
}







