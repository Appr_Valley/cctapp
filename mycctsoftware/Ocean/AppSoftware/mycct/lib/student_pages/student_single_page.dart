
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:mycct/json_folder/json_student_profile.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';


class StudentCardJSONPage extends StatefulWidget {

  @override
  _StudentCardJSONPageState createState() => _StudentCardJSONPageState();
}

class _StudentCardJSONPageState extends State<StudentCardJSONPage> {

  MyPreferences _myPreferences = MyPreferences();

  List<Note> _notes = List<Note>();

  Future<List<Note>> fetchNotes() async {

    final response = await http.post("http://197.242.92.154/rest/tvetcolleges/StudGet", body: {
      "studentno": _myPreferences.username,
    });


    var notes = List<Note>();

    if (response.statusCode == 200) {
      var notesJson = json.decode(response.body);
      for (var noteJson in notesJson) {
        notes.add(Note.fromJson(noteJson));
      }
    }



    return notes;
  }

  @override
  void initState() {
    fetchNotes().then((value) {
      setState(() {
        _notes.addAll(value);
      });
    });
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Your Profile ' + _myPreferences.firstnames,style: TextStyle(color: Colors.white),
          ),
        ),
        body: ListView.builder(
          itemBuilder: (context, index) {
            return Card(
              elevation: 10.0,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(_notes[index].firstname,
                      style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold,fontSize: 20.0),
                    ),

                    Text(_notes[index].surname,
                      style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0),
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: _notes.length,
        )
    );
  }

}


