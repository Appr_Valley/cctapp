import 'package:flutter/material.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:mycct/main.dart';
import 'package:mycct/student_pages/student_prof_progress_page.dart';





class StudMainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new StudMainPageState();
  }
}

class StudMainPageState extends State<StudMainPage> {
  MyPreferences _myPreferences = MyPreferences();

  String msg='';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Hi " + _myPreferences.firstnames),backgroundColor: Colors.redAccent,
        ),
        body: Padding(
          padding: EdgeInsets.all(15),
          child: ListView(
            children: <Widget>[
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50.0,
                    width: 300,
                    child: Material(
                      borderRadius: BorderRadius.circular(30.0),
                      shadowColor: Colors.grey,
                      color: Colors.redAccent,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new ProfPage()));
                        },
                        child: Center(
                          child: Text(
                            'Your Baseline Profile ',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    ),

                  ),
                ],
              ),
              SizedBox(height: 5.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50.0,
                    width: 300,
                    child: Material(
                      borderRadius: BorderRadius.circular(30.0),
                      shadowColor: Colors.grey,
                      color: Colors.redAccent,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          //Navigator.of(context).pop();
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyApp()));
                          //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new LoginScreen1()));
                        },
                        child: Center(
                          child: Text(
                            'EXIT',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });

  }
}
