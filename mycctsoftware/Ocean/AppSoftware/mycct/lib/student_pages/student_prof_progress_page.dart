
import 'package:flutter/material.dart';
import 'package:mycct/shared_preferences/shared_prefs.dart';
import 'package:mycct/local_menus/loggedin_menu.dart';
import 'package:mycct/notes_menus/student_menu.dart';



class ProfPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ProfPageState();
  }
}

class ProfPageState extends State<ProfPage> {
  MyPreferences _myPreferences = MyPreferences();

  int username=0;
  String password='';
  String first_names='';
  String surname='';
  int college_id = 0;
  int campusno = 0;
  int usertypeno = 0;
  String tid = '';

  String msg='';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Welcome - " + _myPreferences.firstnames,
          style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 15.0),),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
              children: <Widget>[
                Text('  Firstname',
                  style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold,fontSize: 12.0),
                  softWrap: true,
                ),
                Card(
                  elevation: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(_myPreferences.firstnames,
                          softWrap: true,
                        ),
                      ],
                    ),
                  ),
                ),
                //const SizedBox(height: 30),
                Text('  Surname',
                  style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold,fontSize: 12.0),
                  softWrap: true,
                ),
                Card(
                  elevation: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(_myPreferences.surname,
                          softWrap: true,
                        ),
                      ],
                    ),
                  ),
                ),
                Text('  Studentno',
                  style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold,fontSize: 12.0),
                  softWrap: true,
                ),
                Card(
                  elevation: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 16.0, right: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(_myPreferences.username,
                          softWrap: true,
                        ),
                      ],
                    ),
                  ),
                ),

                          const SizedBox(height: 30),
                SizedBox(
                  width: 200.0,
                  height: 50.0,
                  child: new RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0)),
                    color: Colors.red,
                    elevation: 7.0,
                    child: new Text(
                      'Exit',
                      style: TextStyle(color: Colors.white,),
                    ),

                    onPressed: () {
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new StudentScreen()));
                    },
                  ),
                ),
              ]
          ),
        )
    );
  }

  @override
  void initState() {
    super.initState();
    _myPreferences.init().then((value) {
      setState(() {
        _myPreferences = value;
      });
    });
  }

}

