<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('DS', DIRECTORY_SEPARATOR);
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require dirname(__FILE__).DS.'vendor'.DS.'autoload.php';

$app = new \Slim\App;


////POST Example.... Get studentno and idno of student
$app->post('/Login', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$postVars = $request->getParams(); ///Gets all the post vars into variable $postVars.....
		$username = $postVars['username'];
		$password = $postVars['password'];
		$data = array();
		
		try {
			$sql = sprintf("SELECT username,password,first_names,surname,college_id,campusno,usertypeno,mobileno FROM system_users WHERE username = '%d' AND password = '%s'",$username,$password);
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$data = $row;
			}
			//var_dump($data);
			
			//data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1;
			//$data['exception'] = $e->getMessage();
		}
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});



////POST Example.... Get Student Details of selected student
$app->post('/StudGet', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$postVars = $request->getParams(); ///Gets all the post vars into variable $postVars.....
		$studentno = $postVars['studentno'];
		$data = array();
		
		try {
			$sql = sprintf("SELECT studentno, qualification_name, idno, first_names, surname, mobileno FROM student_enrolment WHERE student_enrolment.studentno = '%d'",$studentno);
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$data = $row;
			}
			//var_dump($data);
			
			//data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1;
			//$data['exception'] = $e->getMessage();
		}
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});

////POST Example.... Get Student Details of selected student
$app->get('/StudSelect', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$postVars = $request->getParams(); ///Gets all the post vars into variable $postVars.....
		$studentno = $postVars['studentno'];
		$data = array();
		
		try {
			$sql = sprintf("SELECT studentno, qualification_name, idno, first_names, surname, mobileno FROM student_enrolment WHERE student_enrolment.studentno = '%d'",$studentno);
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$data = $row;
			}
			//var_dump($data);
			
			//data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1;
			//$data['exception'] = $e->getMessage();
		}
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});





// Update Guest TokenID
$app->post('/TUpdate', function(Request $request, Response $response){
	$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
	$postVars = $request->getParams(); ///Gets all the post vars into variable $postVars.....
	$username = $postVars['username'];
	$tokenid = $postVars['tokenid'];

	$data = array();
    $sql = sprintf("UPDATE system_users SET tokenid = '%s' WHERE username = '%s'",$tokenid,$username);
	$result = mysqli_query($con,$sql);
	if ($result){
		$data['result'] = 'OK';
	} else {
		$data['result'] = 'ERR';
	}
	$response->getBody()->write(json_encode($data));
	mysqli_close($con);
	return $response;

});

///College Campus Information
$app->get('/Campuses', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$getVars = $request->getParams();  ///Get all the query params in url into varaible $getVars....
		//$studyPeriod = $getVars['period'];
		///If you have more query params, assign them here....
		
		//$data = array(); //Will contain all data and any additional information....
		
		try {
			$sql = sprintf("SELECT * from campuses where college_id = 6 order by campuses.posno");
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$records[] = $row;
			}
			$data = $records;
			//$data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1; ///Query produced ERR...
			//$data['exception'] = $e->getMessage();
		}
		///Use dbResult to check for errors in the client app.....
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});


///College Campus Information
$app->get('/Faqs', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$getVars = $request->getParams();  ///Get all the query params in url into varaible $getVars....
		
		try {
			$sql = sprintf("SELECT * FROM advice_details WHERE advice_details.college_id = 6 ORDER BY advice_details.advice_pos");
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$records[] = $row;
			}
			$data = $records;

		} catch (Exception $e) {

		}
		///Use dbResult to check for errors in the client app.....
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});


///College SRC Council Members
$app->get('/SRCCouncil', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$getVars = $request->getParams();  ///Get all the query params in url into varaible $getVars....
		//$studyPeriod = $getVars['period'];
		///If you have more query params, assign them here....
		
		//$data = array(); //Will contain all data and any additional information....
		
		try {
			$sql = sprintf("SELECT * from src_council where src_council.college_id = 6 order by src_council.posno");
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$records[] = $row;
			}
			$data = $records;
			//$data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1; ///Query produced ERR...
			//$data['exception'] = $e->getMessage();
		}
		///Use dbResult to check for errors in the client app.....
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});


///College SRC Council Members
$app->get('/Council', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$getVars = $request->getParams();  ///Get all the query params in url into varaible $getVars....
		//$studyPeriod = $getVars['period'];
		///If you have more query params, assign them here....
		
		//$data = array(); //Will contain all data and any additional information....
		
		try {
			$sql = sprintf("SELECT id, membername, salutation, pic_url from council_members order by council_members.member_pos");
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$records[] = $row;
			}
			$data = $records;
			//$data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1; ///Query produced ERR...
			//$data['exception'] = $e->getMessage();
		}
		///Use dbResult to check for errors in the client app.....
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});

///College Holidays
$app->get('/Holidays', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$getVars = $request->getParams();  ///Get all the query params in url into varaible $getVars....
		//$studyPeriod = $getVars['period'];
		///If you have more query params, assign them here....
		
		//$data = array(); //Will contain all data and any additional information....
		
		try {
			$sql = sprintf("SELECT * FROM public_holidays WHERE public_holidays.holiday_date >= current_date");
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$records[] = $row;
			}
			$data = $records;
			//$data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1; ///Query produced ERR...
			//$data['exception'] = $e->getMessage();
		}
		///Use dbResult to check for errors in the client app.....
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});

///Academic Calendar
$app->get('/Calendar', function (Request $request, Response $response) {
		$con = mysqli_connect('localhost','dbuser','ruthjul77','tvetcolleges');
		$getVars = $request->getParams();  ///Get all the query params in url into varaible $getVars....
		//$studyPeriod = $getVars['period'];
		///If you have more query params, assign them here....
		
		//$data = array(); //Will contain all data and any additional information....
		
		try {
			$sql = sprintf("SELECT calno,term,staff_commence_date,class_commence_date,class_enddate,exam_date,college_close_date,lecturing_days,lect_staff_days,programtype.programtype AS progdescription FROM academic_calendar LEFT JOIN programtype ON programtype.programtypeid = academic_calendar.programtypeid
ORDER BY programtype.programtype,academic_calendar.term");
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_object($result,'stdClass')){
				$records[] = $row;
			}
			$data = $records;
			//$data['dbResult'] = 1; ///Query was OK...
		} catch (Exception $e) {
			//$data['dbResult'] = -1; ///Query produced ERR...
			//$data['exception'] = $e->getMessage();
		}
		///Use dbResult to check for errors in the client app.....
		$response->getBody()->write(json_encode($data,JSON_NUMERIC_CHECK));
		mysqli_close($con);
		return $response;
});



$app->run();

?>